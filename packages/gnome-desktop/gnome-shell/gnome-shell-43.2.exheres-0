# Copyright 2010-2011 Brett Witherspoon <spoonb@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require gsettings meson
require option-renames [ renames=[ 'systemd journald' ] ]

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gtk-doc
    man-pages
    journald [[ description = [ integrate with journald ] ]]
"

DEPENDENCIES="
    build:
        gnome-desktop/gnome-control-center [[ note = [ for gnome-keybindings.pc ] ]]
        sys-devel/gettext[>=0.19.6]
        virtual/pkg-config[>=0.22]
        gtk-doc? ( dev-doc/gtk-doc[>=1.15] )
        man-pages? ( dev-libs/libxslt )
    build+run:
        core/json-glib
        dev-lang/python:*[>=3] [[ note = [ for gnome-shell-extension-tool ] ]]
        dev-libs/at-spi2-atk [[ note = [ provides atk-bridge which is required ] ]]
        dev-libs/at-spi2-core[gobject-introspection][?X]
        dev-libs/atk
        dev-libs/glib:2[>=2.56.0]
        dev-libs/libsecret:1[>=0.18]
        dev-libs/libxml2:2.0
        gnome-bindings/gjs:1[>=1.73.1]
        gnome-desktop/evolution-data-server:1.2[>=3.33.2][calendar(+)][gobject-introspection]
        gnome-desktop/gcr:4[>=3.90.0][gobject-introspection]
        gnome-desktop/gnome-autoar
        gnome-desktop/gnome-desktop:4[>=3.34.2][gobject-introspection][legacy]
        gnome-desktop/gobject-introspection:1[>=1.49.1]
        gnome-desktop/gsettings-desktop-schemas[>=42_beta]
        gnome-desktop/libsoup:3.0[gobject-introspection]
        gnome-desktop/mutter[>=${PV}][gobject-introspection]
        gps/geoclue:2.0[gobject-introspection]
        inputmethods/ibus[gobject-introspection][>=1.5.19]
        media-sound/pulseaudio[>=2.0]
        net-apps/NetworkManager[>=0.9.8][gobject-introspection]
        net-libs/libnma[>=0.9.6][gobject-introspection] [[ note = [ provides NMA ] ]]
        office-libs/libical:=
        sys-auth/polkit:1[>=0.100][gobject-introspection]
        x11-dri/mesa
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0[gobject-introspection]
        x11-libs/graphene:1.0
        x11-libs/gtk+:3[>=3.15.0][gobject-introspection]
        x11-libs/gtk:4.0
        x11-libs/libX11
        x11-libs/libXfixes
        x11-libs/startup-notification[>=0.11]
        journald? ( sys-apps/systemd )
    run:
        gnome-desktop/gnome-bluetooth:3.0[>=3.9.0][gobject-introspection]
        gnome-desktop/gnome-settings-daemon:3.0
        gnome-desktop/libgnomekbd[gobject-introspection] [[ note = [ for keyboard status applet ] ]]
        gnome-desktop/libgweather:4[gobject-introspection(+)][providers:soup3]
        sys-apps/upower[gobject-introspection]
        x11-libs/pango[gobject-introspection] [[ note = [ silent dependency in userMenu.js ] ]]
    post:
        gnome-desktop/gdm
    recommendation:
        fonts/cantarell-fonts [[
            description = [ The default font for GNOME3 ]
        ]]
        (
            media/pipewire
            media-libs/gstreamer:1.0
            media-plugins/gst-plugins-base:1.0
        ) [[
            description = [ Required for the screen recorder feature ]
        ]]
        media-plugins/gst-plugins-good:1.0[gstreamer_plugins:vpx] [[
            description = [ Required for the screen recorder feature ]
        ]]
        sys-apps/iio-sensor-proxy [[
            description = [ Orientation lock ]
        ]]
        sys-apps/power-profiles-daemon [[
            description = [ Integration to manage power profiles ]
        ]]
        sys-apps/switcheroo-control [[
            description = [ Integration to launch software on discrete GPU ]
        ]]
    suggestion:
        gnome-desktop/gnome-tweaks [[
            description = [ Provides a tool for changing themes ]
        ]]
        gnome-desktop/gnome-shell-extensions [[
            description = [ Extensions to modify and extend GNOME Shell functionality and behavior ]
        ]]
        (
            net-im/telepathy-glib[>=0.17.5][gobject-introspection]
            net-im/telepathy-logger:0.2[>=0.2.4][gobject-introspection]
        ) [[
            description = [ IM integration ]
        ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    '-Dextensions_app=true'
    '-Dextensions_tool=true'
    '-Dnetworkmanager=true'
    -Dsoup2=false
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gtk-doc gtk_doc'
    'journald systemd'
    'man-pages man'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

# Require X
RESTRICT=test

src_prepare() {
    meson_src_prepare
    edo sed -e "/systemduserunitdir/s/prefix, 'lib'/libdir/" -i meson.build
}

