# Copyright 2013 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Copyright 2019 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gtk-icon-cache gsettings vala [ vala_dep=true ]
require python [ with_opt=true blacklist=2 multibuild=false ] meson

SUMMARY="git repository viewer for gtk+/GNOME"
HOMEPAGE="https://git.gnome.org/browse/gitg/"

LICENCES="GPL-2"
SLOT="0"

PLATFORMS="~amd64"

MYOPTIONS="
    debug
    doc
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.17]
        virtual/pkg-config
    build+run:
        base/libgee:0.8[gobject-introspection]
        core/json-glib
        dev-libs/glib:2[>=2.68.0]
        dev-libs/libdazzle:1.0[>=1.0][vapi]
        dev-libs/libpeas:1.0[>=1.5.0]
        dev-libs/libsecret:1[vapi]
        dev-libs/libxml2:2.0[>=2.9.0]
        dev-scm/libgit2-glib:1.0[>=1.0.0][vapi]
        gnome-desktop/gobject-introspection:1[>=0.10.1]
        gnome-desktop/gsettings-desktop-schemas
        gnome-desktop/gspell:1[>=1.8.1][vapi]
        gnome-desktop/gtksourceview:4.0[>=4.0.3]
        x11-libs/gtk+:3[>=3.20.0]
        python? ( gnome-bindings/pygobject:3[>=3.0][python_abis:*(-)?] )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-meson-drop-unused-argument-for-i18n.merge_file.patch
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'debug buildtype debugoptimized release'
    'doc docs'
    'python'
)

src_install() {
    export GSETTINGS_DISABLE_SCHEMAS_COMPILE=1
    meson_src_install
    unset GSETTINGS_DISABLE_SCHEMAS_COMPILE
    option python && python_bytecompile
}

pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

